PrefabFiles = {
	"kami",
	"kami_none",
}

Assets = {
    Asset( "IMAGE", "images/saveslot_portraits/kami.tex" ),
    Asset( "ATLAS", "images/saveslot_portraits/kami.xml" ),

    Asset( "IMAGE", "images/selectscreen_portraits/kami.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/kami.xml" ),
	
    Asset( "IMAGE", "images/selectscreen_portraits/kami_silho.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/kami_silho.xml" ),

    Asset( "IMAGE", "bigportraits/kami.tex" ),
    Asset( "ATLAS", "bigportraits/kami.xml" ),
	
	Asset( "IMAGE", "images/map_icons/kami.tex" ),
	Asset( "ATLAS", "images/map_icons/kami.xml" ),
	
	Asset( "IMAGE", "images/avatars/avatar_kami.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_kami.xml" ),
	
	Asset( "IMAGE", "images/avatars/avatar_ghost_kami.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_kami.xml" ),
	
	Asset( "IMAGE", "images/avatars/self_inspect_kami.tex" ),
    Asset( "ATLAS", "images/avatars/self_inspect_kami.xml" ),
	
	Asset( "IMAGE", "images/names_kami.tex" ),
    Asset( "ATLAS", "images/names_kami.xml" ),
	
    Asset( "IMAGE", "bigportraits/kami_none.tex" ),
    Asset( "ATLAS", "bigportraits/kami_none.xml" ),

}

local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS

-- The character select screen lines
STRINGS.CHARACTER_TITLES.kami = "The Persian"
STRINGS.CHARACTER_NAMES.kami = "Kami"
STRINGS.CHARACTER_DESCRIPTIONS.kami = "*Grows a thicc beard\n*Fasting\n*Can't die from hunger\n*Eats only at evening"
STRINGS.CHARACTER_QUOTES.kami = "\"Myne are fastinger\""

-- Custom speech strings
STRINGS.CHARACTERS.KAMI = require "speech_kami"

-- The character's name as appears in-game 
STRINGS.NAMES.KAMI = "Kami"

AddMinimapAtlas("images/map_icons/kami.xml")

-- Add mod character to mod character list. Also specify a gender. Possible genders are MALE, FEMALE, ROBOT, NEUTRAL, and PLURAL.
AddModCharacter("kami", "MALE")

