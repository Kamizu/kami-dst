# Kami DST

Custom character for the game Don't Stave Together

## Getting Started

Clone or download this project and put the content into Don't Starve Together's mod directory.

Directory locations:

- **Steam/Windows:** C:\Program Files (x86)\Steam\SteamApps\common\dont_starve\mods\

- **Standalone/Windows:** C:\Program Files\dontstarve\mods

- **Standalone/Mac:** /Applications/Don't Starve/Don't Starve.app/Contents/mods/

- **Steam/Linux:** /home/username/.local/share/Steam/SteamApps/common/dont_starve/mods

### Prerequisites

For compiling you will need **Don't Starve Mod Tools** which can be installed on Steam.

### Installing

Launch Don't Starve Together and before you launch your server remember to enable the mod on the server settings!

## Authors

* **Kami Nasri** - *Coding* - [Kamizu](https://bitbucket.org/Kamizu/)

* **Elias Rantanen** - *Art* - [Eg9plant](https://bitbucket.org/Eg9plant/)

## Acknowledgments

This character was made with the help of two forum posts which helps new modders on making their own custom character.

* [Growing custom beard](https://forums.kleientertainment.com/topic/52689-an-idiots-guide-to-growing-custom-facial-hair/)

* [Sample character](https://forums.kleientertainment.com/topic/46849-tutorial-using-extended-sample-character-template/)
