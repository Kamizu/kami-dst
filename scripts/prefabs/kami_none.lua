local assets =
{
	Asset( "ANIM", "anim/kami.zip" ),
	Asset( "ANIM", "anim/ghost_kami_build.zip" ),
	Asset( "ANIM", "anim/kami_beard.zip" ),
}

local skins =
{
	normal_skin = "kami",
	ghost_skin = "ghost_kami_build",
}

local base_prefab = "kami"

local tags = {"KATMIZU", "CHARACTER"}

return CreatePrefabSkin("kami_none",
{
	base_prefab = base_prefab, 
	skins = skins, 
	assets = assets,
	tags = tags,
	
	skip_item_gen = true,
	skip_giftable_gen = true,
})