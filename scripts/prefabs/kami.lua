local MakePlayerCharacter = require "prefabs/player_common"


local assets = {
	Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
	Asset( "ANIM", "anim/kami_beard.zip" ),
}
local prefabs = { "beardhair", }

-- Custom starting items
local start_inv = { }

local function onhungerchange(inst)
	if inst.components.hunger.current <= 1 then
		ApplyFastingStats(inst)
		inst.components.hunger.current = 1
		inst.components.hunger:Pause()
	end

	if inst.components.hunger.current <= inst.components.hunger.max and inst.components.hunger.current > 1 then
		inst.components.hunger:Resume()
	end

	if inst.components.hunger.current == inst.components.hunger.max and inst.components.combat.damagemultiplier == 0 then
		RemoveFastingStats(inst)
		inst.components.hunger:Resume()
	end
end

function ApplyFastingStats(inst)
	inst.components.combat.damagemultiplier = 0		
	inst.components.health:SetAbsorptionAmount(-1)
	inst.components.talker:Say("Myne are fasting. Feelink weak but no looseing health!")
end

function RemoveFastingStats(inst)
	print("removed stats")
	inst.components.health:SetAbsorptionAmount(0)
	inst.components.combat.damagemultiplier = 1
	inst.components.talker:Say("Are weak no more!")
end

-- When the character is revived from human
local function onbecamehuman(inst)
	-- Set speed when reviving from ghost (optional)
	inst.components.locomotor:SetExternalSpeedMultiplier(inst, "kami_speed_mod", 1)
	inst:ListenForEvent("hungerdelta", onhungerchange)
	onhungerchange(inst)
	RemoveFastingStats(inst)
end

local function onbecameghost(inst)
	-- Remove speed modifier when becoming a ghost
   inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "kami_speed_mod")
   inst:RemoveEventCallback("hungerdelta", onhungerchange)
end

-- When loading or spawning the character
local function onload(inst, data)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
	inst:ListenForEvent("ms_becameghost", onbecameghost)
	inst:ListenForEvent("hungerdelta", onhungerchange)

    if inst:HasTag("playerghost") then
		onbecameghost(inst)
	end
	
	print("load")
	if data and data.fasting then
		print(data.fasting)
		print("apply fasting")
		ApplyFastingStats(inst)
	end
end

local function OnSave(inst, data)
	print(inst.components.hunger:IsPaused())
	if inst.components.combat.damagemultiplier == 0 then	
		print("saved true")
		data.fasting = true
	else
		print("saved false")
		data.fasting = false
	end
end


-- This initializes for both the server and client. Tags can be added here.
local common_postinit = function(inst) 
	-- Minimap icon
	inst.MiniMapEntity:SetIcon( "kami.tex" )
end

local function OnResetBeard(inst)
	inst.AnimState:ClearOverrideSymbol("beard")
end 

--tune the beard economy...
local BEARD_DAYS = { 4, 8, 16 } -- default 4, 8, 16
local BEARD_BITS = { 1, 5, 15 } -- default 1, 3, 9

local function OnGrowShortBeard(inst)
	inst.AnimState:OverrideSymbol("beard", "kami_beard", "beard_short")
	inst.components.beard.bits = BEARD_BITS[1]
end

local function OnGrowMediumBeard(inst)
	inst.AnimState:OverrideSymbol("beard", "kami_beard", "beard_medium")
	inst.components.beard.bits = BEARD_BITS[2]
end

local function OnGrowLongBeard(inst)
	inst.AnimState:OverrideSymbol("beard", "kami_beard", "beard_long")
	inst.components.beard.bits = BEARD_BITS[3]
end

-- This initializes for the server only. Components are added here.
local master_postinit = function(inst)
	inst:AddComponent("beard")
	inst.components.beard.onreset = OnResetBeard
	inst.components.beard.prize = "furtuft"
	inst.components.beard:AddCallback(BEARD_DAYS[1], OnGrowShortBeard)
	inst.components.beard:AddCallback(BEARD_DAYS[2], OnGrowMediumBeard)
	inst.components.beard:AddCallback(BEARD_DAYS[3], OnGrowLongBeard)

	-- choose which sounds this character will play
	inst.soundsname = "wilson"
	
	-- Uncomment if "wathgrithr"(Wigfrid) or "webber" voice is used
    --inst.talker_path_override = "dontstarve_DLC001/characters/"
	
	-- Stats	
	inst.components.health:SetMaxHealth(150)
	inst.components.hunger:SetMax(160)
	inst.components.sanity:SetMax(250)

	inst:DoPeriodicTask(1, function(inst)
		if TheWorld.state.isdusk then
			inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
		else
			inst.components.eater:SetDiet({}, {})
		end
	end)
	
	-- Damage multiplier (optional)
    inst.components.combat.damagemultiplier = 1
	
	-- Hunger rate (optional)
	inst.components.hunger.hungerrate = TUNING.WILSON_HUNGER_RATE * 1.4
	
	inst.OnLoad = onload
	inst.OnSave = OnSave
    inst.OnNewSpawn = onload
	
end

return MakePlayerCharacter("kami", prefabs, assets, common_postinit, master_postinit, start_inv)
